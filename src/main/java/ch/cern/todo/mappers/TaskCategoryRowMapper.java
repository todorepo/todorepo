package ch.cern.todo.mappers;

import ch.cern.todo.model.TaskCategory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskCategoryRowMapper implements RowMapper<TaskCategory> {

    public TaskCategory mapRow(ResultSet resultSet, int row) throws SQLException {
        TaskCategory taskCategory = new TaskCategory();
        taskCategory.setCategoryId(resultSet.getInt("CATEGORY_ID"));
        taskCategory.setCategoryName(resultSet.getString("CATEGORY_NAME"));
        taskCategory.setCategoryDescription(resultSet.getString("CATEGORY_DESCRIPTION"));

        return taskCategory;
    }
}
