package ch.cern.todo.mappers;

import ch.cern.todo.model.Task;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskRowMapper implements RowMapper<Task> {

    public Task mapRow(ResultSet resultSet, int row) throws SQLException {
        Task task = new Task();
        task.setTaskId(resultSet.getInt("TASK_ID"));
        task.setTaskName(resultSet.getString("TASK_NAME"));
        task.setTaskDescription(resultSet.getString("TASK_DESCRIPTION"));
        task.setDeadline(resultSet.getTimestamp("DEADLINE"));
        task.setCategoryId(resultSet.getInt("CATEGORY_ID"));

        return task;
    }
}
