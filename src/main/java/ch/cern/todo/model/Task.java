package ch.cern.todo.model;

import java.sql.Timestamp;

public class Task {

    private int taskId;
    private String taskName;
    private String taskDescription;
    private Timestamp deadline;
    private int categoryId;

    public Task(){

    }

    public Task(int taskId, String taskName, String taskDescription, Timestamp deadline, int categoryId){
        this.taskId = taskId;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.deadline = deadline;
        this.categoryId = categoryId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Timestamp getDeadline() {
        return deadline;
    }

    public void setDeadline(Timestamp deadline) {
        this.deadline = deadline;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "{" +
                "taskId=" + taskId +
                ", taskName=" + taskName +
                ", taskDescription=" + taskDescription +
                ", deadline=" + deadline +
                ", categoryId=" + categoryId +
                "}";
    }
}
