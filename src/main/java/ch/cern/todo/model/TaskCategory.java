package ch.cern.todo.model;

public class TaskCategory {

    private int categoryId;
    private String categoryName;
    private String categoryDescription;

    public TaskCategory(){

    }

    public TaskCategory(int categoryId, String categoryName, String categoryDescription){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    @Override
    public String toString() {
        return "TaskCategory {" +
                "categoryId=" + categoryId +
                ", categoryName=" + categoryName +
                ", categoryDescription=" + categoryDescription +
                "}";
    }
}
