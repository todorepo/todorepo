package ch.cern.todo.service;

import ch.cern.todo.dao.TaskDao;
import ch.cern.todo.exceptions.CreationException;
import ch.cern.todo.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService{

    @Autowired
    private TaskDao taskDao;

    public TaskDao getTaskDao() {
        return taskDao;
    }

    @Override
    public Task getTask(int taskId){
        return taskDao.getTask(taskId);
    }

    @Override
    public List<Task> getAllTasks(){
        return taskDao.getAllTasks();
    }

    @Override
    public Task createTask(Task task){
        try {
            int dbResponse = 0;

            if(task.getTaskName() == null || task.getDeadline() == null || task.getCategoryId() == 0){
                return null;
            }

            dbResponse = taskDao.createTask(task);

            if (dbResponse == 0) {
                throw new CreationException("Error creating task.");
            }
            return taskDao.getTask(dbResponse);
        }catch (CreationException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Task editTask(Task task){
        try {
            int dbResponse = 0;

            dbResponse = taskDao.editTask(task);

            if (dbResponse == 0) {
                throw new CreationException("Error editing task");
            }
            return taskDao.getTask(task.getTaskId());
        }catch (CreationException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteTask(int taskId){
        return taskDao.deleteTask(taskId);
    }
}
