package ch.cern.todo.service;

import ch.cern.todo.dao.TaskCategoryDao;
import ch.cern.todo.exceptions.CreationException;
import ch.cern.todo.model.TaskCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskCategoryServiceImpl implements TaskCategoryService {

    @Autowired
    private TaskCategoryDao taskCategoryDao;

    public TaskCategoryDao getTaskCategoryDao() {
        return taskCategoryDao;
    }

    @Override
    public TaskCategory getTaskCategory(int categoryId) {
        return taskCategoryDao.getTaskCategory(categoryId);
    }

    @Override
    public List<TaskCategory> getAllTaskCategories() {
        return taskCategoryDao.getAllTaskCategories();
    }

    @Override
    public ResponseEntity<Object> createTaskCategory(TaskCategory taskCategory) {
        try {
            int dbResponse = 0;

            dbResponse = taskCategoryDao.createTaskCategory(taskCategory);

            return new ResponseEntity<>(taskCategoryDao.getTaskCategory(dbResponse),HttpStatus.OK);
        } catch (CreationException e) {
            return new ResponseEntity<>("Error, category with this category name already exists.", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public TaskCategory editTaskCategory(TaskCategory taskCategory) {
        try {
            int dbResponse = 0;

            if(taskCategory.getCategoryName() == null){
                return null;
            }

            dbResponse = taskCategoryDao.editTaskCategory(taskCategory);

            if (dbResponse == 0) {
                throw new CreationException("Error editing category");
            }
            return taskCategoryDao.getTaskCategory(taskCategory.getCategoryId());
        } catch (CreationException e) {
            return null;
        }
    }

    @Override
    public List<Integer> deleteTaskCategory(int categoryId) {

        return taskCategoryDao.deleteTaskCategory(categoryId);
    }
}

