package ch.cern.todo.service;

import ch.cern.todo.model.TaskCategory;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TaskCategoryService {

    public TaskCategory getTaskCategory(int categoryId);

    public List<TaskCategory> getAllTaskCategories();

    public ResponseEntity<Object> createTaskCategory(TaskCategory taskCategory);

    public TaskCategory editTaskCategory(TaskCategory taskCategory);

    public List<Integer> deleteTaskCategory(int categoryId);
}
