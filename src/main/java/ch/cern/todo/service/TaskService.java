package ch.cern.todo.service;

import ch.cern.todo.model.Task;

import java.util.List;

public interface TaskService {

    public Task getTask(int taskId);

    public List<Task> getAllTasks();

    public Task createTask(Task task);

    public Task editTask(Task task);

    public int deleteTask(int taskId);
}
