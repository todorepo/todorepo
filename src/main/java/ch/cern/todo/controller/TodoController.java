package ch.cern.todo.controller;

import ch.cern.todo.SwaggerConfig;
import ch.cern.todo.model.Task;
import ch.cern.todo.model.TaskCategory;
import ch.cern.todo.service.TaskCategoryService;
import ch.cern.todo.service.TaskService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Api(tags = SwaggerConfig.TODO_API)
public class TodoController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskCategoryService taskCategoryService;

    @GetMapping(value = "/getTask", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> getTask(int taskId){

        logger.info("Attempting to retrieve task id: "+taskId);
        Task task = taskService.getTask(taskId);

        if(task == null){
            return new ResponseEntity<>("Task with taskId "+taskId+" not found.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(taskService.getTask(taskId), HttpStatus.OK);
    }

    @GetMapping(value = "/getAllTasks", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> getAllTasks(){

        logger.info("Attempting to retrieve all tasks...");

        List<Task> taskList = taskService.getAllTasks();

        if(taskList.isEmpty()){
            return new ResponseEntity<>("No tasks in database", HttpStatus.OK);
        }

        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    @GetMapping(value = "/getTaskCategory", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> getTaskCategory(int categoryId){

        logger.info("Attempting to retrieve category: "+categoryId);
        TaskCategory taskCategory = taskCategoryService.getTaskCategory(categoryId);

        if(taskCategory == null){
            return new ResponseEntity<>("Category with categoryId "+categoryId+" not found.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(taskCategoryService.getTaskCategory(categoryId), HttpStatus.OK);
    }

    @GetMapping(value = "/getAllTaskCategories", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> getAllTaskCategories(){

        logger.info("Attempting to retrieve all categories...");

        List<TaskCategory> categoryList = taskCategoryService.getAllTaskCategories();

        if(categoryList.isEmpty()){
            return new ResponseEntity<>("No categories in database", HttpStatus.OK);
        }

        return new ResponseEntity<>(taskCategoryService.getAllTaskCategories(), HttpStatus.OK);
    }

    @PostMapping(path = "tasks",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createTask(@RequestBody Task task){

        logger.info("Attempting to create task: "+task.getTaskName());
        Task newTask = taskService.createTask(task);

        if (newTask == null){
           return new ResponseEntity<>("Error creating task. Please check your input is valid.", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(newTask, HttpStatus.CREATED);
    }

    @PostMapping(path = "taskCategories",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createTaskCategory(@RequestBody TaskCategory taskCategory){

        logger.info("Attempting to create category: "+taskCategory.getCategoryName());
        return taskCategoryService.createTaskCategory(taskCategory);
    }

    @PutMapping("/tasks/{taskId}")
    public ResponseEntity<Object> editTask(@PathVariable(value = "taskId") int taskId, @RequestBody Task taskDetails){

        logger.info("Attempting to edit task with id: "+ taskId);
        Task task = taskService.getTask(taskId);

        if(task == null){
            return new ResponseEntity<>("TaskId: "+ taskId +" does not exist.", HttpStatus.NOT_FOUND);
        }

        Optional.ofNullable(taskDetails.getTaskName()).ifPresent(task::setTaskName);
        Optional.ofNullable(taskDetails.getTaskDescription()).ifPresent(task::setTaskDescription);
        Optional.ofNullable(taskDetails.getDeadline()).ifPresent(task::setDeadline);
        //if categoryId is not provided, it will be set to 0 by default, so we only overwrite if not 0
        if(taskDetails.getCategoryId() != 0){
            task.setCategoryId(taskDetails.getCategoryId());
        }

        final Task updatedTask = taskService.editTask(task);

        logger.info("Successfully updated task with id: " + updatedTask.getTaskId());
        return new ResponseEntity<>(updatedTask, HttpStatus.OK);
    }

    @PutMapping(value = "/taskCategories/{categoryId}")
    public ResponseEntity<Object> editTaskCategory(@PathVariable (value = "categoryId") int categoryId, @RequestBody TaskCategory categoryDetails){

        logger.info("Attempting to edit category with id: "+ categoryId);
        TaskCategory category = taskCategoryService.getTaskCategory(categoryId);

        if(category == null){
            return new ResponseEntity<>("CategoryId: "+ categoryId +" does not exist.", HttpStatus.NOT_FOUND);
        }

        Optional.ofNullable(categoryDetails.getCategoryName()).ifPresent(category::setCategoryName);
        Optional.ofNullable(categoryDetails.getCategoryDescription()).ifPresent(category::setCategoryDescription);

        final TaskCategory updatedCategory = taskCategoryService.editTaskCategory(category);

        logger.info("Successfully updated category with id: " + updatedCategory.getCategoryId());
        return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteTask", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> deleteTask(int taskId){

        logger.info("Attempting to delete task with id: "+ taskId);
        Task taskToDelete = taskService.getTask(taskId);

        if (taskToDelete == null){
            return new ResponseEntity<>("Unable to delete Task id: "+ taskId +", does not exist.", HttpStatus.NOT_FOUND);
        }

        taskService.deleteTask(taskId);

        logger.info("Successfully deleted task with id: "+ taskId);
        return new ResponseEntity<>("Success deleting task id: "+ taskId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteTaskCategory", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> deleteTaskCategory(int categoryId){

        logger.info("Attempting to delete category with id: "+ categoryId);
        TaskCategory categoryToDelete = taskCategoryService.getTaskCategory(categoryId);

        if (categoryToDelete == null){
            return new ResponseEntity<>("Unable to delete Category id: "+ categoryId +", does not exist.", HttpStatus.NOT_FOUND);
        }

        List<Integer> tasksUsingCategory = taskCategoryService.deleteTaskCategory(categoryId);

        if(!tasksUsingCategory.isEmpty()){
            return new ResponseEntity<>("Unable to delete Category id: "+categoryId+", category still in use by taskIds :"+tasksUsingCategory, HttpStatus.BAD_REQUEST);
        }

        logger.info("Successfully deleted category with id: "+ categoryId);
        return new ResponseEntity<>("Success deleting category id: "+ categoryId, HttpStatus.OK);
    }
}
