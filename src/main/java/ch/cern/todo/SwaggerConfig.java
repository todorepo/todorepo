package ch.cern.todo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String TODO_API = "Todo Application REST API";

    public ApiInfo DEFAULT_API_INFO(){
        return new ApiInfoBuilder()
                .title("Todo Application")
                .description("This application can be used to create, retrieve, edit or delete Tasks and Task Categories\n" +
                        "Use \"YYYY-MM-DDTHH:mm:SS\" for timestamp fields")
                .build();
    }

    private static final Set<String> APPLICATION_JSON = new HashSet<String>(Arrays.asList(MediaType.APPLICATION_JSON_VALUE));

   @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .apiInfo(DEFAULT_API_INFO())
                .tags(new Tag(TODO_API, "This is the TODO API"))
                .produces(APPLICATION_JSON)
                .consumes(APPLICATION_JSON)
                .select()
                .paths(PathSelectors.any())
                .build().directModelSubstitute(Timestamp.class,String.class);
   }
}
