package ch.cern.todo.dao;

import ch.cern.todo.model.Task;

import java.util.List;

public interface TaskDao {

    public Task getTask(int taskId);

    public List<Task> getAllTasks();

    public int createTask(Task task);

    public int editTask(Task task);

    public int deleteTask(int taskId);
}
