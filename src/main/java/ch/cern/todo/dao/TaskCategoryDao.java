package ch.cern.todo.dao;

import ch.cern.todo.exceptions.CreationException;
import ch.cern.todo.model.TaskCategory;

import java.util.List;

public interface TaskCategoryDao {

    public TaskCategory getTaskCategory(int categoryId);

    public List<TaskCategory> getAllTaskCategories();

    public int createTaskCategory(TaskCategory taskCategory) throws CreationException;

    public int editTaskCategory(TaskCategory taskCategory);

    public List<Integer> deleteTaskCategory(int categoryId);
}
