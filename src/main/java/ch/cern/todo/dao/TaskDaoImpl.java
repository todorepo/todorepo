package ch.cern.todo.dao;

import ch.cern.todo.mappers.TaskRowMapper;
import ch.cern.todo.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TaskDaoImpl implements TaskDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final TaskRowMapper taskRowMapper = new TaskRowMapper();

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Transactional
    public Task getTask(int taskId) throws EmptyResultDataAccessException {
        try {
            Task task = jdbcTemplate.queryForObject("SELECT * FROM TASKS WHERE TASK_ID = ?", taskRowMapper, taskId);
            return task;
        }catch (EmptyResultDataAccessException e){
            logger.error("Error retrieving task ID: "+taskId+". This task does not exist.");
            return null;
        }
    }

    @Transactional
    public List<Task> getAllTasks(){
        List<Task> tasks = jdbcTemplate.query("SELECT * FROM TASKS",
                taskRowMapper);
        return tasks;
    }

    @Transactional
    public int createTask(Task task){
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("TASKS").usingGeneratedKeyColumns("TASK_ID");

        Map<String, Object> parameters = new HashMap<String, Object>(4);
        parameters.put("TASK_NAME", task.getTaskName());
        parameters.put("TASK_DESCRIPTION", task.getTaskDescription());
        parameters.put("DEADLINE", task.getDeadline());
        parameters.put("CATEGORY_ID", task.getCategoryId());

        int taskId = jdbcInsert.executeAndReturnKey(parameters).intValue();
        return taskId;
    }

    @Transactional
    public int editTask(Task task){
        String editQueryString = "UPDATE TASKS SET TASK_NAME = ?, TASK_DESCRIPTION = ?, DEADLINE = ?, CATEGORY_ID = ? WHERE TASK_ID = ?";
        int response = jdbcTemplate.update(editQueryString, task.getTaskName(), task.getTaskDescription(), task.getDeadline(),
                task.getCategoryId(), task.getTaskId());
        return response;
    }

    @Transactional
    public int deleteTask(int taskId){
        int response = jdbcTemplate.update("DELETE FROM TASKS WHERE TASK_ID = ?", taskId);
        return response;
    }
}
