package ch.cern.todo.dao;

import ch.cern.todo.exceptions.CreationException;
import ch.cern.todo.mappers.TaskCategoryRowMapper;
import ch.cern.todo.mappers.TaskRowMapper;
import ch.cern.todo.model.Task;
import ch.cern.todo.model.TaskCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TaskCategoryDaoImpl implements TaskCategoryDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final TaskCategoryRowMapper taskCategoryRowMapper = new TaskCategoryRowMapper();
    private final TaskRowMapper taskRowMapper = new TaskRowMapper();

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Transactional
    public TaskCategory getTaskCategory(int taskCategoryId){
        try{
            TaskCategory taskCategory = jdbcTemplate.queryForObject("SELECT * FROM TASK_CATEGORIES WHERE CATEGORY_ID = ?", taskCategoryRowMapper, taskCategoryId);
            return taskCategory;
        }catch (EmptyResultDataAccessException e){
            logger.error("Error retrieving category ID: "+taskCategoryId+". This category does not exist.");
            return null;
        }
    }

    @Transactional
    public List<TaskCategory> getAllTaskCategories(){
        List<TaskCategory> taskCategories = jdbcTemplate.query("SELECT * FROM TASK_CATEGORIES",
                taskCategoryRowMapper);
        return taskCategories;
    }

    @Transactional
    public int createTaskCategory(TaskCategory taskCategory) throws CreationException{

        List<TaskCategory> taskCategories = jdbcTemplate.query("SELECT * FROM TASK_CATEGORIES WHERE CATEGORY_NAME = ?", taskCategoryRowMapper, taskCategory.getCategoryName());
        if(!taskCategories.isEmpty()){
            throw new CreationException("Error, this category name already exists");
        }

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("TASK_CATEGORIES").usingGeneratedKeyColumns("CATEGORY_ID");

        Map<String, Object> parameters = new HashMap<String, Object>(4);
        parameters.put("CATEGORY_NAME", taskCategory.getCategoryName());
        parameters.put("CATEGORY_DESCRIPTION", taskCategory.getCategoryDescription());

        int categoryId = jdbcInsert.executeAndReturnKey(parameters).intValue();
        return categoryId;
    }

    @Transactional
    public int editTaskCategory(TaskCategory taskCategory){
        String editQueryString = "UPDATE TASK_CATEGORIES SET CATEGORY_NAME = ?, CATEGORY_DESCRIPTION = ? WHERE CATEGORY_ID = ?";
        int response = jdbcTemplate.update(editQueryString, taskCategory.getCategoryName(), taskCategory.getCategoryDescription(), taskCategory.getCategoryId());
        return response;
    }

    @Transactional
    public List<Integer> deleteTaskCategory(int categoryId){

        List<Integer> taskIds = new ArrayList<>();

        //check if category is used by any tasks before deleting
        List<Task> tasks = jdbcTemplate.query("SELECT * FROM TASKS WHERE CATEGORY_ID = ?", taskRowMapper, categoryId);
        for (Task task: tasks) {
            taskIds.add(task.getTaskId());
        }

        if(!taskIds.isEmpty()){
            logger.info("Attempt to delete categoryId: "+categoryId+" failed. Category still in use by: "+tasks);
        }else{
            logger.info("Deleting categoryId: "+categoryId);
            jdbcTemplate.update("DELETE FROM TASK_CATEGORIES WHERE CATEGORY_ID = ?", categoryId);
        }
        return taskIds;
    }
}
