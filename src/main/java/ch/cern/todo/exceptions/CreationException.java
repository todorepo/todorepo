package ch.cern.todo.exceptions;

public class CreationException extends Exception{

    public CreationException(String message) { super(message); }
}
