INSERT INTO TASK_CATEGORIES(`CATEGORY_ID`, `CATEGORY_NAME`, `CATEGORY_DESCRIPTION`) VALUES(1, 'First Category', 'This is the first category');

INSERT INTO TASK_CATEGORIES(`CATEGORY_ID`, `CATEGORY_NAME`, `CATEGORY_DESCRIPTION`) VALUES(2, 'Category 2', 'Second Category');

INSERT INTO TASKS(`TASK_ID`,`TASK_NAME`,`TASK_DESCRIPTION`,`DEADLINE`,`CATEGORY_ID`) VALUES (1, 'Create a task', 'This is the first task', {ts '2022-02-12 15:36:23.35'}, 1);

INSERT INTO TASKS(`TASK_ID`,`TASK_NAME`,`TASK_DESCRIPTION`,`DEADLINE`,`CATEGORY_ID`) VALUES (2, 'Add another task', 'Task 2: the sequel', {ts '2022-02-14 18:54:24.33'}, 2);