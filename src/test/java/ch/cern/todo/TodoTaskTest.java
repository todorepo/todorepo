package ch.cern.todo;

import ch.cern.todo.controller.TodoController;
import ch.cern.todo.model.Task;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD) //resets db state after each test
public class TodoTaskTest {

    @Autowired
    private TodoController todoController;

    @Test
    public void getTaskTest(){

        ResponseEntity<Object> successfulResponse = todoController.getTask(1);
        assertEquals(200, successfulResponse.getStatusCodeValue());

        //additional improvement on input validation would be to reject negative taskId values
        ResponseEntity<Object> notFoundResponse = todoController.getTask(-2);
        assertEquals(404, notFoundResponse.getStatusCodeValue());
    }

    @Test
    public void getAllTasksTest(){

        ResponseEntity<Object> successfulResponse = todoController.getAllTasks();
        assertEquals(200, successfulResponse.getStatusCodeValue());
    }

    @Test
    public void createTaskTest(){

        String testTaskName = "test taskName";
        String testTaskDescription = "test taskDescription";
        Timestamp testDeadlineTimestamp = Timestamp.valueOf("2022-02-15 12:30:00.0");
        int testCategoryId = 1;

        Task newTask = new Task();
        newTask.setTaskName(testTaskName);
        newTask.setTaskDescription(testTaskDescription);
        newTask.setDeadline(testDeadlineTimestamp);
        newTask.setCategoryId(testCategoryId);

        ResponseEntity<Object> successfulResponse = todoController.createTask(newTask);
        Task createdTask = (Task) successfulResponse.getBody();

        assertNotNull(createdTask);
        assertEquals(testTaskName, createdTask.getTaskName());
        assertEquals(testTaskDescription, createdTask.getTaskDescription());
        assertEquals(testDeadlineTimestamp, createdTask.getDeadline());
        assertEquals(testCategoryId, createdTask.getCategoryId());
    }

    @Test
    public void invalidCreateTaskTest(){

        Task badTask = new Task();

        ResponseEntity<Object> failedResponse = todoController.createTask(badTask);

        assertEquals(400, failedResponse.getStatusCodeValue());

    }

    @Test
    public void editTaskTest(){

        int taskIdToEdit = 1;
        Task taskDetails = new Task();
        String editedTaskName = "edited Task Name";
        String editedTaskDescription = "edited Task Description";
        Timestamp editedTimestamp = Timestamp.valueOf("2022-02-15 12:30:00.0");
        int editedCategoryId = 2;

        taskDetails.setTaskName(editedTaskName);
        taskDetails.setTaskDescription(editedTaskDescription);
        taskDetails.setDeadline(editedTimestamp);
        taskDetails.setCategoryId(editedCategoryId);

        ResponseEntity<Object> editedResponse = todoController.editTask(taskIdToEdit, taskDetails);

        Task editedTask = (Task) editedResponse.getBody();
        assertNotNull(editedTask);
        assertEquals(editedTaskName, editedTask.getTaskName());
        assertEquals(editedTaskDescription, editedTask.getTaskDescription());
        assertEquals(editedTimestamp, editedTask.getDeadline());
        assertEquals(editedCategoryId, editedTask.getCategoryId());

    }

    @Test
    public void deleteTaskTest(){

        int taskIdToDelete = 1;

        ResponseEntity<String> deleteResponse = todoController.deleteTask(taskIdToDelete);
        assertEquals(200, deleteResponse.getStatusCodeValue());

        ResponseEntity<String> deleteNonExistingResponse = todoController.deleteTask(taskIdToDelete);
        assertEquals(404, deleteNonExistingResponse.getStatusCodeValue());
    }
}
