package ch.cern.todo;

import ch.cern.todo.controller.TodoController;
import ch.cern.todo.model.TaskCategory;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD) //resets db state after each test
public class TodoTaskCategoryTest {

    @Autowired
    private TodoController todoController;

    @Test
    public void getTaskCategoryTest(){

        ResponseEntity<Object> successfulResponse = todoController.getTaskCategory(1);
        assertEquals(200, successfulResponse.getStatusCodeValue());

        //additional improvement on input validation would be to reject negative categoryId values
        ResponseEntity<Object> notFoundResponse = todoController.getTaskCategory(-2);
        assertEquals(404, notFoundResponse.getStatusCodeValue());
    }

    @Test
    public void getAllTaskCategoriesTest(){

        ResponseEntity<Object> successfulResponse = todoController.getAllTaskCategories();
        assertEquals(200, successfulResponse.getStatusCodeValue());
    }

    @Test
    public void createTaskCategoryTest(){

        String testCategoryName = "test categoryName";
        String testCategoryDescription = "test categoryDescription";

        TaskCategory newCategory = new TaskCategory();
        newCategory.setCategoryName(testCategoryName);
        newCategory.setCategoryDescription(testCategoryDescription);

        ResponseEntity<Object> successfulResponse = todoController.createTaskCategory(newCategory);
        TaskCategory createdCategory = (TaskCategory) successfulResponse.getBody();

        assertNotNull(createdCategory);
        assertEquals(testCategoryName, createdCategory.getCategoryName());
        assertEquals(testCategoryDescription, createdCategory.getCategoryDescription());

        ResponseEntity<Object> duplicateResponse = todoController.createTaskCategory(newCategory);
        assertEquals(400, duplicateResponse.getStatusCodeValue());

    }

    @Test
    public void editTaskCategoryTest(){

        int categoryIdToEdit = 1;
        TaskCategory categoryDetails = new TaskCategory();
        String editedCategoryName = "edited Category Name";
        String editedCategoryDescription = "edited Category Description";


        categoryDetails.setCategoryName(editedCategoryName);
        categoryDetails.setCategoryDescription(editedCategoryDescription);

        ResponseEntity<Object> editedResponse = todoController.editTaskCategory(categoryIdToEdit, categoryDetails);

        TaskCategory editedTaskCategory = (TaskCategory) editedResponse.getBody();
        assertNotNull(editedTaskCategory);
        assertEquals(editedCategoryName, editedTaskCategory.getCategoryName());
        assertEquals(editedCategoryDescription, editedTaskCategory.getCategoryDescription());
    }

    @Test
    public void deleteTaskCategoryTest(){

        int categoryIdToDelete = 1;

        //first attempt, category is in use by task id 1
        ResponseEntity<String> deleteInUseResponse = todoController.deleteTaskCategory(categoryIdToDelete);
        assertEquals(400, deleteInUseResponse.getStatusCodeValue());

        todoController.deleteTask(1);

        //seconds attempt, successfully deleted
        ResponseEntity<String> deleteResponse = todoController.deleteTaskCategory(categoryIdToDelete);
        assertEquals(200, deleteResponse.getStatusCodeValue());

        //third attempt, category no longer exists
        ResponseEntity<String> deleteNonExistingResponse = todoController.deleteTaskCategory(categoryIdToDelete);
        assertEquals(404, deleteNonExistingResponse.getStatusCodeValue());
    }
}
